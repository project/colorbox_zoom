Colorbox Zoom

Zoom in on an image that has been loaded through Colorbox. This is great if you
have a high resolution image that'd you'd like to show more detail.

This integrates Jack Moore's jQuery Zoom plugin with Drupal Colorbox.
Demo & more info: http://www.jacklmoore.com/zoom/

Zoom is triggered on hover by default and can changed to grab, click, or toggle
- zooming over the part of the image where the mouse is at. Override options in
the module's js/options.js file.

## Requirements

- Colorbox

## Installation

- Download Colorbox (and it's dependencies) from Drupal.org
- Download Colorbox Zoom from Drupal.org
- Enable Colorbox Zoom
- Open a Colorbox and hover over the image
- Override options in the js/options.js file as needed
